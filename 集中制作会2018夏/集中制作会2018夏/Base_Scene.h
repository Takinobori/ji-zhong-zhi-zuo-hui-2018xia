#pragma once
#include "Keyboard.h"

enum eScene
{
	eScene_Title, eScene_Game, eScene_Result,
	eScene_Num,
};

struct Base_Scene
{
	static eScene next_scene;	// 『今のシーン』
	static eScene now_scene;	// 『次のシーン』
	static bool Check_Scene();	// 二つのシーンが異なるかどうか確認する
};