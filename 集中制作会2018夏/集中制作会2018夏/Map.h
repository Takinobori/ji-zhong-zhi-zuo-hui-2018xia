#pragma once
#include <DxLib.h>
#include <vector>
#include <array>
#include <string>
#include <fstream>
#include <sstream>
#include <bitset>

class Map
{
private:
	std::array<int, 5>image;
	std::ifstream ifs;
	std::ofstream ofs;
	void Load_File();
	void Load_Graph();
public:
	int x; 
	int y;
	Map();
	~Map();
	std::vector<std::vector<int>>data;
	void Draw();
	int GetSize_X();
	int GetSize_Y();
	int Get_x();
	int Get_y();
};

