#pragma once
#include "Base_Character.h"
#include "Keyboard.h"

struct tPlayer
{
	
	Base_Character base;
	void Initialize(tPoint p, tSize s, Muki muki);
	void Delete();
	void Update();
	void Draw(int add_x,int add_y);
};