#include "Base_Character.h"

Base_Character Base_Character::Get_Base_Character(tPoint p, tSize s, Muki muki)
{
	Base_Character base_character;
	base_character.point = p;
	base_character.size = s;
	base_character.muki = muki;
	return base_character;
}
void Base_Character::Set_Back()
{
	back_point = point;
}
void Base_Character::Go_Back()
{
	point = back_point;
}
int Base_Character::Get_Center_x()
{
	int center_x;
	center_x = point.x + size.w / 2;
	return center_x;
}
int Base_Character::Get_Center_y()
{
	int center_y;
	center_y = point.y + size.h / 2;
	return center_y;
}
