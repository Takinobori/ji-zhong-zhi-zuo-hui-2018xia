#pragma once

struct tSize
{
	int w;
	int h;
	static tSize Get_Size(int w, int h);
};