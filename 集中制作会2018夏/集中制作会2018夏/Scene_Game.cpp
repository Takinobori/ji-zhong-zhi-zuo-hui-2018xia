#include "Scene_Game.h"

void Scene_Game::Initialize() 
{
	player.Initialize(tPoint::Get_Point(128, 128), tSize::Get_Size(64, 64), UP);
	father.Initialize(tPoint::Get_Point(192, 192), tSize::Get_Size(64, 64), UP);
	map = new Map();
}
void Scene_Game::Delete() 
{
	player.Delete();
	father.Delete();
	delete map;
}
void Scene_Game::Update() 
{
	// シーンの移動処理(デバック用)
	{
		if (Keyboard::Get(KEY_INPUT_SPACE) == 1)
		{
			Base_Scene::next_scene = eScene_Result;
		}
		if (Keyboard::Get(KEY_INPUT_BACK) == 1)
		{
			Base_Scene::next_scene = eScene_Title;
		}
	}
	player.Update(map);
}
void Scene_Game::Draw()
{
	map->Draw();
	player.Draw(map);
	// デバック用文章
	{
		DrawFormatString(64, 64, 0xffffff, "ゲーム画面です。");
		DrawFormatString(64, 64 * 2, 0xffffff, "バックスペースでタイトル画面に戻ります。");
		DrawFormatString(64, 64 * 3, 0xffffff, "スペースキーでリザルト画面に移行します。");
		DrawFormatString(64, 64 * 5, 0xff5050, "プレイヤーらしきものを追加。");
		DrawFormatString(64, 64 * 6, 0xff5050, "十字キーで対応の方向へ移動。");
		DrawFormatString(64, 64 * 7, 0xff5050, "画像はまだない。");
		DrawFormatString(64, 64 * 8, 0xff5050, "ついでにコメントアウトもプレイヤーは書いてない。頑張って書くます。");
	}
}