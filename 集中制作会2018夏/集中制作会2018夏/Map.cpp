#include "Map.h"



Map::Map()
{
	Load_Graph();
	Load_File();
	x = (1920 - (signed)data[0].size() * 64) / 2;
	y = (1080 - (signed)data.size() * 64) / 2;
}
Map::~Map()
{
	for (int i = 0, n = (signed)image.size(); i < n; i++)
	{
		DeleteGraph(image[i]);
	}
}
void Map::Load_File()
{
	ifs.open("Map/0.txt");
	std::string str_one;
	std::string str_w;
	int X = 0;
	int Y = 0;
	while (getline(ifs, str_w))
	{
		X = 0;
		std::istringstream iss(str_w);
		data.resize(Y + 1);
		while (getline(iss, str_one, ','))
		{
			data[Y].push_back(stoi(str_one));
			X++;
		}
		Y++;
	}
	ifs.close();
}
void Map::Load_Graph()
{
	std::string pass;
	for (int i = 0, n = (signed)image.size(); i < n; i++)
	{
		pass = "image/" + std::to_string(i) + ".png";
		image[i] = LoadGraph(pass.c_str());
	}
}
void Map::Draw()
{
	for (int i = 0, n = (signed)data.size(); i < n; i++)
	{
		for (int j = 0, o = (signed)data[i].size(); j < o; j++)
		{
			if (data[i][j] != 0)
			{
				DrawGraph(x + j * 64, y + i * 64, image[data[i][j] - 1], true);
			}
		}
	}
}
int Map::GetSize_X()
{
	return (signed)data[0].size();
}
int Map::GetSize_Y()
{
	return (signed)data.size();
}
int Map::Get_x()
{
	return x;
}
int Map::Get_y()
{
	return y;
}
