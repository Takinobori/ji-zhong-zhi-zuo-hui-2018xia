#include "Player.h"

void tPlayer::Initialize(tPoint p, tSize s, Muki muki)
{
	base = Base_Character::Get_Base_Character(p, s, muki);
}
void tPlayer::Delete(){}
void tPlayer::Update()
{
	base.Set_Back();
	if (Keyboard::Get(KEY_INPUT_UP) > 0)
	{
		base.point.y -= 3;
		base.muki = UP;
	}
	else if (Keyboard::Get(KEY_INPUT_LEFT) > 0)
	{
		base.point.x -= 3;
		base.muki = LEFT;
	}
	else if (Keyboard::Get(KEY_INPUT_DOWN) > 0)
	{
		base.point.y += 3;
		base.muki = DOWN;
	}
	else if (Keyboard::Get(KEY_INPUT_RIGHT) > 0)
	{
		base.point.x += 3;
		base.muki = RIGHT;
	}
}
void tPlayer::Draw(int add_x, int add_y)
{
	DrawBox(add_x+base.point.x, add_y+base.point.y, add_x+base.point.x + base.size.w, add_y+base.point.y + base.size.h, 0xffffff, true);
}

