#pragma once
#include "Scene_Title.h"
#include "Scene_Game.h"
#include "Scene_Result.h"


struct SceneMgr
{
	// 各シーン、おそらく今回はこれで全て
	Scene_Title  title;
	Scene_Game   game;
	Scene_Result result;

	void Initialize();		// 初期化、画像の読み込みとか
	void Delete();			// 終了処理、画像のメモリ開放とか
	void Update();			// 更新、挙動
	void Draw();			// 描画
	void Change_Scene();	// 『now_scene』から『next_scene』に移行する
};