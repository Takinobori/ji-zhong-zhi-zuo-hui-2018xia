#include "Base_Scene.h"

eScene Base_Scene::now_scene;
eScene Base_Scene::next_scene;

bool Base_Scene::Check_Scene()
{
	// 『今のシーン』と『次のシーン』が違うならばtrue、そうでないならばfalseを返す
	// 『今のシーン』と『次のシーン』が違うならばシーンを切り替える
	return now_scene != next_scene;
}