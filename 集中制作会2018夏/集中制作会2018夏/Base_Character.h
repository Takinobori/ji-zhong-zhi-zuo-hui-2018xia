#pragma once
#include "Point.h"
#include "Size.h"

enum Muki
{
	UP, LEFT, DOWN, RIGHT, 
};
struct Base_Character
{
	tPoint point;
	tPoint back_point;
	tSize size;
	Muki muki;
	static Base_Character Get_Base_Character(tPoint p, tSize s, Muki muki);
	void Set_Back();
	void Go_Back();
	int Get_Center_x();
	int Get_Center_y();
};