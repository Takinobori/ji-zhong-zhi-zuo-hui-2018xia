#pragma once
#include "Player.h"
#include "Map.h"

struct PlayerMgr
{
	void Initialize(tPoint p, tSize s, Muki muki);
	void Delete();
	tPlayer player;
	void Update(Map* map);
	void Draw(Map* map);
};