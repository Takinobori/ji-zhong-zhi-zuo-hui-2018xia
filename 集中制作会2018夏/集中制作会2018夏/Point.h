#pragma once
#include "math.h"

struct tPoint
{
	int x;
	int y;
	static tPoint Get_Point(int x, int y);
	double length(tPoint another);
	double length_H(tPoint another);
	double length_W(tPoint another);
};