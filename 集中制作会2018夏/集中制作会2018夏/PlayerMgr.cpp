#include "PlayerMgr.h"

void PlayerMgr::Initialize(tPoint p, tSize s, Muki muki)
{
	player.Initialize(p, s, muki);
}
void PlayerMgr::Delete()
{
	player.Delete();
}
void PlayerMgr::Update(Map* map)
{
	player.Update();
	if (map->data[player.base.Get_Center_y() / PANEL][player.base.Get_Center_x() / PANEL] != 1)
	{
		player.base.Go_Back();
	}
}
void PlayerMgr::Draw(Map* map)
{
	player.Draw(map->x,map->y);
}